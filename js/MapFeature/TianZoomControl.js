import {TianMapFeature} from "./TianMapFeature.js";

/**
 * @prop {Tian} map
 * @prop {Object} settings
 * @prop {String} settings.type
 * @prop {String} settings.position
 */
export default class TianZoomControl extends TianMapFeature {
  constructor(settings, map) {
    super(settings, map);

    let control = new T.Control.Zoom();

    const positionMapping = {
      "T_ANCHOR_TOP_LEFT": "topleft",
      "T_ANCHOR_TOP_RIGHT": "topright",
      "T_ANCHOR_BOTTOM_LEFT": "bottomleft",
      "T_ANCHOR_BOTTOM_RIGHT": "bottomright",
    };
    control.setPosition(positionMapping[this.settings.position]);
    this.map.tianMap.addControl(
      control
    );
  }
}
