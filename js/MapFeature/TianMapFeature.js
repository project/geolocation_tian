import { GeolocationMapFeature } from "../../../geolocation/js/MapFeature/GeolocationMapFeature.js";

/**
 * @prop {Tian} map
 */
export class TianMapFeature extends GeolocationMapFeature {
  /**
   * @param {TianMapMarker} marker
   *   Tian marker.
   */
  onMarkerAdded(marker) {
    super.onMarkerAdded(marker);
  }

  /**
   * @param {TianMapMarker} marker
   *   Tian marker.
   */
  onMarkerUpdated(marker) {
    super.onMarkerUpdated(marker);
  }

  /**
   * @param {TianMapMarker} marker
   *   Tian marker.
   */
  onMarkerRemove(marker) {
    super.onMarkerRemove(marker);
  }

  /**
   * @param {TianMapMarker} marker
   *   Tian marker.
   */
  onMarkerClicked(marker) {
    super.onMarkerClicked(marker);
  }
}
