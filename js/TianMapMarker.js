import { GeolocationMapMarker } from "../../geolocation/js/Base/GeolocationMapMarker.js";
import { GeolocationCoordinates } from "../../geolocation/js/Base/GeolocationCoordinates.js";

/**
 * @prop {T.Marker} tianMarker
 * @prop {Tian} map
 */
export class TianMapMarker extends GeolocationMapMarker {
  constructor(coordinates, settings = {}, map = null) {
    super(coordinates, settings, map);

    const tianMarkerSettings = {
      title: this.settings.title,
    };

    if (this.settings.icon) {
      tianMarkerSettings.icon = new T.Icon(this.settings.icon, new T.Size(300, 157));
    }

    this.tianMarker = new T.Marker(new T.LngLat(coordinates.lng, coordinates.lat), tianMarkerSettings);

    if (this.settings.label) {
      // this.tianMarker.setLabel(new T.Label(this.settings.label));
    }

    this.tianMarker.addEventListener("click", () => {
      this.click();
    });

    if (this.settings.draggable) {
      this.tianMarker.enableDragging();
      this.tianMarker.addEventListener("dragend", (e) => {
        this.update(new GeolocationCoordinates(Number(e.point.lng), Number(e.point.lat)));
      });
    }
  }

  update(newCoordinates, settings) {
    super.update(newCoordinates, settings);

    if (newCoordinates) {
      if (!newCoordinates.equals(this.tianMarker.getPosition().lat, this.tianMarker.getPosition().lng)) {
        this.tianMarker.setPosition(new T.LngLat(newCoordinates.lng, newCoordinates.lat));
      }
    }

    if (this.settings.title) {
      this.tianMarker.setTitle(this.settings.title);
    }
    if (this.settings.label) {
      this.tianMarker.setLabel(new T.Label(this.settings.label));
    }
    if (this.settings.icon) {
      this.tianMarker.setIcon(new T.Icon(this.settings.icon, new T.Size(300, 157)));
    }
  }

  remove() {
    super.remove();

    this.map.tianMap.removeOverlay(this.tianMarker);
  }
}
