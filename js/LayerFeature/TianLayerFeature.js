import { GeolocationLayerFeature } from "../../../geolocation/js/LayerFeature/GeolocationLayerFeature.js";

/**
 * @prop {Tian} layer.map
 */
export class TianLayerFeature extends GeolocationLayerFeature {
  /**
   * @param {TianMapMarker} marker
   *   Tian marker.
   */
  onMarkerAdded(marker) {}

  /**
   * @param {TianMapMarker} marker
   *   Tian marker.
   */
  onMarkerRemove(marker) {}

  /**
   * @param {TianMapMarker} marker
   *   Tian marker.
   */
  onMarkerUpdated(marker) {}

  /**
   * @param {TianMapMarker} marker
   *   Tian marker.
   */
  onMarkerClicked(marker) {}
}
