import { TianLayerFeature } from "./TianLayerFeature.js";

/**
 * @prop {Tian} map
 * @prop {Object} settings
 * @prop {String} settings.type
 * @prop {String} settings.position
 */
export default class TianMarkerInfoWindow extends TianLayerFeature {
  onMarkerClicked(marker) {
    super.onMarkerClicked(marker);

    this.layer.map.tianMap.openInfoWindow(
      new T.InfoWindow(marker.getContent(), {
        width: 200,
        height: 100,
        title: marker.title,
      }),
      marker.tianMarker.getPosition()
    );
  }
}
