<?php

namespace Drupal\geolocation_tian\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements the Tian Maps form controller.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class TianMapsSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->configFactory->get('geolocation_tian.settings');

    $form['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tian Maps App ID'),
      '#default_value' => $config->get('key'),
      '#description' => $this->t('Tian Maps requires users to sign up at <a href="@link_tian_api" target="_blank">Tian Map API Key</a>.', [
        '@link_tian_api' => 'https://console.tianditu.gov.cn/api/key',
      ]),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'geolocation_tian_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'geolocation_tian.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->configFactory()->getEditable('geolocation_tian.settings');
    $config->set('key', $form_state->getValue('key'));

    $config->save();

    // Confirmation on form submission.
    \Drupal::messenger()->addMessage($this->t('The configuration options have been saved.'));

    drupal_flush_all_caches();
  }

}
