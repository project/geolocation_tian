<?php

namespace Drupal\geolocation_tian\Plugin\geolocation\MapFeature;

use Drupal\geolocation\Plugin\geolocation\MapFeature\ControlElementBase;

/**
 * Provides map zoom control support.
 *
 * @MapFeature(
 *   id = "tian_zoom_control",
 *   name = @Translation("Tian Zoom control"),
 *   description = @Translation("Add map zoom controls."),
 *   type = "tian",
 * )
 */
class TianZoomControl extends ControlElementBase {}
