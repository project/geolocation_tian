<?php

namespace Drupal\geolocation_tian\Plugin\geolocation\LayerFeature;

use Drupal\geolocation\LayerFeatureBase;

/**
 * Provides marker infowindow.
 *
 * @LayerFeature(
 *   id = "tian_marker_infowindow",
 *   name = @Translation("Marker InfoWindow"),
 *   description = @Translation("Open InfoWindow on Marker click."),
 *   type = "tian",
 * )
 */
class TianMarkerInfoWindow extends LayerFeatureBase {}
